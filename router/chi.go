package router

import (
	"net/http"
	"strconv"
	"strings"

	"github.com/go-chi/chi"
)

type Router interface {
	Mount(pattern string, handler http.Handler)
	Get(pattern string, handler http.HandlerFunc)
	Post(pattern string, handler http.HandlerFunc)
	Put(pattern string, handler http.HandlerFunc)
	Delete(pattern string, handler http.HandlerFunc)
	Route(pattern string, f func(Router)) Router
	Use(middlewares ...func(http.Handler) http.Handler)

	http.Handler
}

type router struct {
	mux chi.Router
}

func New() Router {
	return router{chi.NewRouter()}
}

func (rr router) Mount(pattern string, handler http.Handler) {
	rr.mux.Mount(pattern, handler)
}

func (rr router) Get(pattern string, handler http.HandlerFunc) {
	rr.mux.Get(pattern, handler)
}

func (rr router) Post(pattern string, handler http.HandlerFunc) {
	rr.mux.Post(pattern, handler)
}

func (rr router) Put(pattern string, handler http.HandlerFunc) {
	rr.mux.Put(pattern, handler)
}

func (rr router) Delete(pattern string, handler http.HandlerFunc) {
	rr.mux.Delete(pattern, handler)
}

func (rr router) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	rr.mux.ServeHTTP(w, r)
}

func (rr router) Use(middlewares ...func(http.Handler) http.Handler) {
	rr.mux.Use(middlewares...)
}

func (rr router) Route(pattern string, f func(Router)) Router {
	g := func(c chi.Router) {
		f(router{c})
	}
	return router{rr.mux.Route(pattern, g)}
}

/**
 * Returns a (lowercase) string from the path, as found in
 *  the request path.
 *
 * If the parameter is not found, returns an empty string
 */
func StringParam(r *http.Request, paramName string) string {

	return strings.ToLower(chi.URLParam(r, paramName))
}

/**
 *  Retrieves the named parameter from the request context
 *
 *  PANIC if the param is not a valid integer (or is missing)
 */
func IntParam(r *http.Request, paramName string) int {
	str := chi.URLParam(r, paramName)
	x, err := strconv.Atoi(str)
	if err != nil {
		panic("Expected " + paramName + " [ " + str + " ] to be an integer. " + err.Error())
	}
	return x
}
