package main

import (
	"bitbucket.org/atlassian/go-stack/log"
	"bitbucket.org/atlassian/go-stack/router"
	"fmt"
	"io"
	"net/http"
)

var LOG, _ = log.NewProductionLogger()

func hello(w http.ResponseWriter, r *http.Request) {
	name := router.StringParam(r, "name")
	io.WriteString(w, "Hello "+name)
}

func helloWorld(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "Hello World")
}

func simulatePanic(w http.ResponseWriter, r *http.Request) {
	panic("The second edition of the critically a claimed hitch hikers guide to the galaxy.")
}

func main() {
	r := router.New()
	r.Use(log.WithRequestLogger(log.DefaultRequestContextFactory(LOG)))
	r.Use(log.LogPanic)
	r.Use(log.AccessLogger)
	r.Get("/", helloWorld)
	r.Get("/panic", simulatePanic)
	r.Route(
		"/hello",
		func(r router.Router) {
			r.Get("/world/{name:[a-zA-Z]+}", hello)
		},
	)
	fmt.Println(http.ListenAndServe(":3000", r).Error())
}


