package log

import (
	"bitbucket.org/atlassian/go-stack"
	"encoding/hex"
	"fmt"
	"go.uber.org/zap"
	"runtime"
	"time"
)

func Duration(key string, val time.Duration) gostack.Field {
	return zap.Duration(key, val)
}

func Int(key string, val int) gostack.Field {
	return zap.Int(key, val)
}

func Error(val error) gostack.Field {
	return zap.Error(val)
}

func String(key string, val string) gostack.Field {
	return zap.String(key, val)
}

func Stringer(key string, val fmt.Stringer) gostack.Field {
	return zap.Stringer(key, val)
}

func Hex(key string, val []byte) gostack.Field {
	return zap.String(key, hex.EncodeToString(val))
}

type callerStringer struct {
	skip int
}

func (s callerStringer) String() string {
	_, file, line, ok := runtime.Caller(s.skip + 1)
	if !ok {
		return "<top of stack reached>"
	}
	return fmt.Sprintf("%s:%d", file, line)
}

func Caller(skip int) gostack.Field {
	return NamedCaller("caller", skip+7)
}

func NamedCaller(name string, skip int) gostack.Field {
	return zap.Stringer(name, callerStringer{skip})
}

func NewProductionLogger(opts ...gostack.Option) (gostack.Logger, error) {
	return zap.NewProductionConfig().Build(opts...)
}

func NewDevelopmentLogger(opts ...gostack.Option) (gostack.Logger, error) {
	return zap.NewDevelopmentConfig().Build(opts...)
}
