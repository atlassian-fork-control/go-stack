package log

import (
	"bitbucket.org/atlassian/go-stack"
	"context"
	"fmt"
	"github.com/google/uuid"
	"go.uber.org/zap/zapcore"
	"net/http"
	"reflect"
)

func ContextLogger(r *http.Request) gostack.Logger {
	if LOG, ok := r.Context().Value(logCtxKey).(gostack.Logger); ok {
		return LOG
	}
	return nil
}

func LogPanic(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		LOG := ContextLogger(r)
		defer func() {
			if p := recover(); p != nil {
				var field zapcore.Field
				switch e := p.(type) {
				case error:
					field = Error(e)
				case string:
					field = String("error", e)
				case fmt.Stringer:
					field = Stringer("error", e)
				default:
					field = Stringer("error-type", reflect.TypeOf(p))
				}
				LOG.Info("request-panic", field)
				w.WriteHeader(500)
				return
			}
		}()
		handler.ServeHTTP(w, r)
	})
}

func AccessLogger(f http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		LOG := ContextLogger(r)
		LOG.Info("Access")
		f.ServeHTTP(w, r)
	})
}

func WithRequestLogger(lFac RequestLoggerFactory) gostack.Middleware {
	return func(handler http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			LOG := lFac.New(r)
			defer LOG.Sync()
			handler.ServeHTTP(w, withLoggerContext(r, LOG))
		})
	}
}

type RequestLoggerFactory interface {
	New(r *http.Request) gostack.Logger
}

func DefaultRequestContextFactory(l gostack.Logger) RequestLoggerFactory {
	return defaultRequestContextFactory{l}
}

type defaultRequestContextFactory struct {
	inner gostack.Logger
}

func (LOG defaultRequestContextFactory) New(r *http.Request) gostack.Logger {
	id, _ := uuid.NewRandom()
	return LogWithCtx(
		LOG.inner,
		Stringer("request.id", id),
		String("request.method", r.Method),
		String("request.path", r.URL.Path),
		Caller(0),
	)
}

func withLoggerContext(r *http.Request, LOG gostack.Logger) *http.Request {
	baseCtx := r.Context()
	logCtx := context.WithValue(baseCtx, logCtxKey, LOG)
	return r.WithContext(logCtx)
}

var logCtxKey = "net.atlassian.go.stack.log.context"
