package log

import (
	"bitbucket.org/atlassian/go-stack/mocks"
	"bytes"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
	"net/http"
	"net/http/httptest"
	"testing"
)

func Test_panic_logger(t *testing.T) {
    controller := gomock.NewController(t)
    defer controller.Finish()
    LOG := mock_stack.NewMockLogger(controller)

    f := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
        panic("hello")
    })
    g := WithRequestLogger(DefaultRequestContextFactory(LOG))(LogPanic(f))

    w:= httptest.NewRecorder()
    r, _ := http.NewRequest("GET", "/", &bytes.Buffer{})

    LOG.EXPECT().Info("request-panic", String("error", "hello"))
    LOG.EXPECT().Sync()

    g.ServeHTTP(w, r)
    assert.Equal(t, 500, w.Code)
}

func Test_request_context(t *testing.T) {
	controller := gomock.NewController(t)
	defer controller.Finish()
	LOG := mock_stack.NewMockLogger(controller)

	f := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctxLogger := ContextLogger(r)
		ctxLogger.Info("hello")
	})
	g := WithRequestLogger(DefaultRequestContextFactory(LOG))(LogPanic(f))

	w:= httptest.NewRecorder()
	r, _ := http.NewRequest("POST", "/path/to/servlet", &bytes.Buffer{})

	LOG.EXPECT().Info("hello",
		gomock.Any(),
		String("request.method", "POST"),
		String("request.path", "/path/to/servlet"),
		Caller(0),
	)
	LOG.EXPECT().Sync()

	g.ServeHTTP(w, r)
	assert.Equal(t, 200, w.Code)
}

type SyncedBuffer struct {
	*bytes.Buffer
}

func (SyncedBuffer) Sync() error {
	return nil
}

func Test_caller_field_base_skip(t *testing.T) {
	t.Skipf("This is blocked on upstream contribution to zap")
	return

	buf:=&bytes.Buffer{}
	LOG, err := NewDevelopmentLogger(zap.ErrorOutput(SyncedBuffer{buf}))
	assert.NoError(t, err)

	LOG.Info("hello", Caller(0))
	LOG.Sync()
	assert.Equal(t, "\tINFO\tlog/middleware_test.go:72\thello\t{\"caller\": \"C:/Users/kpratt/go/src/bitbucket.org/atlassian/go-stack/log/middleware_test.go:72\"}", string(buf.Bytes()))
}