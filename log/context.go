package log

import (
	"bitbucket.org/atlassian/go-stack"
	"go.uber.org/zap/zapcore"
)

func LogWithCtx(LOG gostack.Logger, fields ...zapcore.Field) gostack.Logger {
	return &contextLogger{
		raw:       LOG,
		additions: fields,
	}
}

type contextLogger struct {
	raw       gostack.Logger
	additions []zapcore.Field
}

func (LOG *contextLogger) Info(msg string, fields ...zapcore.Field) {
	fields = append(fields, LOG.additions...)
	LOG.raw.Info(msg, fields...)
}

func (LOG *contextLogger) Debug(msg string, fields ...zapcore.Field) {
	fields = append(fields, LOG.additions...)
	LOG.raw.Debug(msg, fields...)
}

func (LOG *contextLogger) Sync() error {
	return LOG.raw.Sync()
}