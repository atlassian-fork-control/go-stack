package jwt

import (
	"context"
	"net/http"
	"github.com/dgrijalva/jwt-go"
	"fmt"
	"io"
	"bitbucket.org/atlassian/go-stack/log"
)

type contextKey string

type Claims interface {
	Valid() error
}

func RequestWithSignedObject(req *http.Request, sigStamp []byte, obj Claims) error {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, obj)
	sig, err := token.SignedString(sigStamp)
	if err != nil {
		return err
	}
	req.Header.Add("Authorization", "Bearer "+sig)
	return nil
}

func GetClaims(r *http.Request) Claims {
	return r.Context().Value(contextKey("jwt-claims")).(Claims)
}

/**
	Http middleware. If the jwt is valid it will be found in:

 		r.Context().Value(contextKey("jwt-claims"))
 */
func RequireJwtToken(
	secret string,
	claimsFactory func() Claims,
) (func(next http.Handler) http.Handler) {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			LOG := log.ContextLogger(r)
			var tokenStr string
			auth := r.Header.Get("Authorization")
			if len(auth) >= 7 && auth[:7] == "Bearer " {
				tokenStr = auth[7:]
			} else if len(auth) >= 4 && auth[:4] == "JWT " {
				tokenStr = auth[4:]
			} else if jwt := r.URL.Query().Get("jwt"); jwt != "" {
				tokenStr = jwt
			}
			if tokenStr == "" {
				w.WriteHeader(http.StatusForbidden)
				io.WriteString(w, "Requires valid token")
			}

			claims := claimsFactory()
			token, err := jwt.ParseWithClaims(tokenStr, claims, func(token *jwt.Token) (interface{}, error) {
				method, ok := token.Method.(*jwt.SigningMethodHMAC)
				if !ok || method.Name != "HS256" {
					LOG.Info("unexpected signing", log.Stringer("method", token.Header["alg"].(fmt.Stringer)))
					return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
				}
				return []byte(secret), nil
			})

			if err != nil || !token.Valid {
				w.WriteHeader(http.StatusForbidden)
				io.WriteString(w, "Requires valid token")
			}

			r = r.WithContext(context.WithValue(r.Context(), contextKey("jwt-claims"), token.Claims))
			next.ServeHTTP(w, r)
			if LOG != nil {
				LOG.Sync()
			}
			return
		})
	}
}

func (c contextKey) String() string {
	return "context key " + string(c)
}
